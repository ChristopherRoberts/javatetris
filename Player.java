public class Player {
    /* the current score of the player */
    private int curScore = 0;
    private int highScore = 0; //the high score of the player
    private int curPiecesCleared = 0; //how many pieces the player has cleared this round
    
    public Player() {
        initPlayer();
    }

    private void initPlayer() {
        this.curScore = 0;
        this.highScore = 0;
        this.curPiecesCleared = 0;
    }
    public void setCurScore(int newScore) {
        this.curScore = newScore;
    }
    public void updateCurScore (int additionalScore) {
        this.curScore += additionalScore;
    }
    public void setHighScore(int newScore) {
        if(newScore > highScore) {
            this.highScore = newScore;
        }
    }
    public void setPiecesCleared (int newClearedCount) {
        this.curPiecesCleared = newClearedCount;
    }
    public void updatePiecesCleared(int clearedCount) {
        this.curPiecesCleared += clearedCount;
    }
    public int getCurScore() {
        return curScore;
    }
    public int getHighScore() {
        return highScore;
    }
    public int getPiecesCleared() {
        return curPiecesCleared;
    }
}