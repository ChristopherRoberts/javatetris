import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
//import java.io.Console; //Enable this if you would like to print on the console for debugging

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import sun.java2d.pipe.SpanClipRenderer;

public class Board extends JPanel 
        implements ActionListener {
	/*define constants for customizing the game */
	public  final int BOARD_WIDTH = 10; //number of rows in the board
	public  final int BOARD_HEIGHT = 20; //number of columns
	public final int CELL_WIDTH = 30;
	public final int CELL_HEIGHT = 35;	
    public  final char SPACE = ' ';
    public  final String TARGET = "355";
    
    private final int DELAY = 400;
    private static final Color BLACK =  new Color(0,0,0);
    
    private Timer timer;  //We use the java.util.Timer to create a game cycle. The shapes move on a square by square basis (not pixel by pixel).
    private boolean isFallingFinished = false; //isFallingFinished is set to true when the fall of a piece is completed; it is set to false when the next piece starts to fall. 
    private boolean isStarted = false;  //isStarted is set to false when the board height reaches the limit and the next piece can't be moved (i.e., game is over). The program exist when isStarted becomes false. 
    private boolean isPaused = false;   //set to true when the game is paused; false otherwise.
    private JLabel statusbar; 
    private JLabel scorebar;  //The label displaying the game score
    private JLabel scoretext; 
    private Font textFont;
    private Piece curPiece;   //The current (falling) piece
    private char [][] board; //The board grid
    private Player curPlayer;
    
    public Board(Tetris parent) {

        initBoard(parent);
    }
    
    /*clear all cells on the board. A cell in the board is considered empty if the character at that cell is SPACE. Iniatially all cells are initialized with SPACE.*/
    private void clearBoard() {
        for (int i = 0; i < BOARD_HEIGHT; i++) {
        	for (int j = 0; j < BOARD_WIDTH; j++) {
        		board[j][i] = SPACE;
        	}
        }
    }
    
    /*calculate the cell width and height in pixels*/
    private int squareWidth() { return (int) getSize().getWidth() / BOARD_WIDTH; }
    private int squareHeight() { return (int) getSize().getHeight() / BOARD_HEIGHT; }
    
    /*calculate the X coordinate of the left upper corner of j th cell on a row. (j is a 0-based index)*/  
    private int calcX(int j) {
    	return (j * squareWidth());
        
    }
    /*calculate the Y coordinate of the left upper corner of i th cell on a column. (i is a 0-based index)*/  
    private int calcY(int i) {
        Dimension size = getSize();
    	int boardTop = (int) size.getHeight() - BOARD_HEIGHT * squareHeight(); 
    	return (boardTop + i * squareHeight());
        
    }
    /*initialize the game board, create the first piece, and start the timer */
    private void initBoard(Tetris parent) {
        
       setFocusable(true);
       curPiece = new Piece();
       //create the Timer. The board object is registered as the listener  for the timer. When the timer is fired, Board's action listener method will be called. 
       timer = new Timer(DELAY, this);
       timer.start(); 
       curPlayer = new Player();

   	   this.textFont = new Font("Helvatica", Font.BOLD, 22);
       statusbar = parent.getStatusBar(); //get the reference to the Tetris's status bar
       scorebar = parent.getScoreBar();   //get the reference to the Tetris's score bar
       scoretext = parent.getScoreText();   //get the reference to the Tetris's score bar
       
       scoretext.setFont(textFont);
       scorebar.setBorder(BorderFactory.createEtchedBorder(1));
       scorebar.setFont(textFont);
       statusbar.setFont(textFont);
       
       addKeyListener(new TAdapter());
       //create and clear the board
       board = new char [BOARD_WIDTH] [BOARD_HEIGHT];
       clearBoard();    
       
    }

    /* When Timer is fired, Board's override of actionPerformed will be called*/
    @Override
    public void actionPerformed(ActionEvent e) {
        /*if the falling of the current piece is completed, create a new piece*/
        if (isFallingFinished) {        
            isFallingFinished = false;
            createNewPiece();
        } else{ // else move the current piece one level down on the board.     
            oneLineDown();
        }
    }

/* Start the game. */  
    public void start()  {
        
        if (isPaused)
            return;

        isStarted = true;
        isFallingFinished = false;
        
        createNewPiece();
        timer.start();
        clearBoard();
    }
    /* Pause the game. */  
    private void pause()  {
        
        if (!isStarted)
            return;

        isPaused = !isPaused;
        
        if (isPaused) {
            
            timer.stop();
            statusbar.setText("PAUSED");
        } else {
            
            timer.start();
            //changed statusbar to display the pieces cleared after unpausing
            statusbar.setText(String.valueOf(curPlayer.getPiecesCleared()));
        }
        
        repaint();
    }

    /* draw a square over the non-empty board cells (a cell is non-empty if its value is not SPACE). The color of the cell is determined based on the cell value. */
    private void drawSquare(Graphics g, int x, int y, char number)  {
        
        Color colors[] = { new Color(50, 150, 50), new Color(204, 102, 102), 
            new Color(102, 204, 102), new Color(102, 102, 204), 
            new Color(204, 204, 102), new Color(204, 102, 204), 
            new Color(0, 255, 0), new Color(0, 0, 255),
            new Color(102, 204, 204), new Color(218, 170, 0)
        };

        Color color = colors[Character.getNumericValue(number)];

        g.setColor(color);
        g.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2);
        g.setColor(BLACK);
        g.setFont(textFont);
        g.drawString(String.valueOf(number), x+(squareWidth()-(CELL_WIDTH/2)+1)/2, y+(squareHeight()+(CELL_HEIGHT/2))/2 );
             
        g.setColor(color.brighter());
        g.drawLine(x, y + squareHeight() - 1, x, y);
        g.drawLine(x, y, x + squareWidth() - 1, y);

        g.setColor(color.darker());
        g.drawLine(x + 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + squareHeight() - 1);
        g.drawLine(x + squareWidth() - 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + 1);

    }

    /*draw the squares for the non empty cells on the board (already dropped pieces and the current piece currently falling)*/
    private void doDrawing(Graphics g) {       
   	
    	/*draw the squares for already dropped pieces*/
        for (int i = 0; i < BOARD_HEIGHT; i++) {
            for (int j = 0; j < BOARD_WIDTH; j++) {
                if (board[j][i] != SPACE) {
                	drawSquare(g, calcX(j),calcY(i), board[j][i]);
                }
            }
        }
        /*draw the square for the currently dropping piece*/
        if (curPiece.getNumber() != SPACE) {                
                drawSquare(g, calcX(curPiece.getX()),calcY(curPiece.getY()), curPiece.getNumber());
            
        }        
    }

    /*Board's overwrite for paintComponent. This will be called every time "repaint() is called . */
    @Override
    public void paintComponent(Graphics g) { 

        super.paintComponent(g);
        doDrawing(g);
    }

    /*move the current piece all the way down on the board*/
    private void dropDown() {
        
        int newY = curPiece.getY();
        
        while (newY < BOARD_HEIGHT-1) {
            
            if (!tryMove(curPiece, 0 , 1))
                break;
            ++newY;
        }
        
        pieceDropped();
    }
   
    /*move the current piece one row down on the board*/
    private void oneLineDown()  {
        
        if (!tryMove(curPiece, 0, 1))
            pieceDropped();
    }

    /*this should fill the gaps with pieces that need to fall*/
    private boolean dropAllLineDown() {
        boolean result = false;
        for (int i = (BOARD_HEIGHT - 2); i > 0; i--) {
            for (int j = 0; j < BOARD_WIDTH; j++) {
                if (board[j][i] != SPACE && board[j][i + 1] == SPACE) {
                    board[j][i+1] = board[j][i];
                    board[j][i] = SPACE;   
                    result = true;          
                }
            }
        }
        return result;
    }

    /*checks if the fall of the current piece is completed. 
     * **TODO** If so, updates the board and the score.*/
    private void pieceDropped() {                
        updateBoard(curPiece); 

        if (!isFallingFinished)
            createNewPiece();
    }

    /*creates the new piece; if the board is full (i.e, can't move the current piece) stops the game. */  
    private void createNewPiece()  {
        
        curPiece.setRandomNumber();
        curPiece.setX(BOARD_WIDTH / 2 + 1);
        curPiece.setY(0);

        if (!tryMove(curPiece, 0, 0)) {
            
            curPiece.setNumber(SPACE);
            timer.stop();
            isStarted = false;
            statusbar.setText("GAME OVER");
        }
    }

    /*moves the current piece by newX,newY on the Board and repaints the Board*/
    private boolean tryMove(Piece newPiece, int newX, int newY) {
        int x = newPiece.getX() + newX;
        int y = newPiece.getY() + newY;
           
        if (x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT)
        	return false;     
        
        if (board[x][y] != SPACE)
            return false;
       

        curPiece = newPiece;
        curPiece.setX(x);
        curPiece.setY(y);

        repaint();

        return true;
    }
    
    /*Updates the Board: 
     * clears all cell groups including 3 (or more) matching adjacent cells
     * clears all cell groups including the target characters 
     *     - can appear either horizontal(left to right or right to left )  or vertical (up to down or down to up)
     *     - the characters of the target string need to appear in the same order.*/

     //additionally adds the count of pieces cleared to the player class checks
     //and updates the player score
    public void updateBoard(Piece curPiece) {
        
        //init the data to default state
        int count = 0;
        int check = 0;
        int score = 0;
        int targetClearedCount = 0;
        int scoreTarget = 0; 
        boolean hasCleared = false;

        //update the board at the current piece
        board[curPiece.getX()][curPiece.getY()] = curPiece.getNumber();
        
        //we check to see if there is a target match first because it is worth more points
        //then clear the matches
        scoreTarget = checkForTarget(curPiece);

        if (scoreTarget > 1) {
            hasCleared = true;
            score += scoreTarget;
            targetClearedCount = scoreTarget / 20;
            targetClearedCount = targetClearedCount * 3;
            count += targetClearedCount;
            updateCount(count);
        }

        //reset the count so we are not adding wrong information
        count = 0;
        count += checkCur();

        if(count > 1) {
            hasCleared = true;
            count++;
            score += (count * 5);
        }

        /*dropAlllLineDown returns true if pieces needed to be dropped. 
        then gets the score of all pieces cleared after their drop
        */
        while (hasCleared) {
            hasCleared = dropAllLineDown();
            check = checkAll();
            score += check;
        }

        if(count > 2) {
            updateCount(count);
        }

        score += checkAll();

        if(count > 2 || score > 0) {
            updateScore(score);
        }

        //update the screen to reflect the score and pieces cleared
        statusbar.setText(String.valueOf(curPlayer.getPiecesCleared()));
        scorebar.setText(String.valueOf(curPlayer.getCurScore()));
    }
     
    //used to help debug, would print out the cords of the current piece and the count of
    //matching pieces
    private void printCords(int x, int y, int count) {
        System.out.print("cords are: ");
        System.out.print(x);
        System.out.print(" ");
        System.out.print(y);
        System.out.print(" the count is: ");
        System.out.println(count);
    }

    //functions to update the player class
    private void updateCount(int piecesCleared) {
        curPlayer.updatePiecesCleared(piecesCleared);
    }

    private void updateScore(int additionalScore) {
        curPlayer.updateCurScore(additionalScore);
    }

    //checks the falling piece for adjacent matches
    private int checkCur () {
        int count = 0;

        count += checkAdjacentLeft(curPiece);
        count += checkAdjacentRight(curPiece);
        count += checkAdjacentBelow(curPiece);
        count += checkAdjacentAbove(curPiece);

        // printCords(curPiece.getX(), curPiece.getY(), count);
        if (count > 1) {
            if(checkLeft(curPiece) == 1) {
                board[curPiece.getX() -1][curPiece.getY()] = SPACE;
            }

            if(checkRight(curPiece) == 1) {
                board[curPiece.getX() +1][curPiece.getY()] = SPACE;
            }

            if(checkBelow(curPiece) == 1) {
                board[curPiece.getX()][curPiece.getY() +1] = SPACE;
            }

            if(checkAbove(curPiece) == 1) {
                board[curPiece.getX()][curPiece.getY() -1] = SPACE;
            }

            board[curPiece.getX()][curPiece.getY()] = SPACE;
        }
        return count;
     }

     //checks every piece on the board to see if they have adjacent matches or a target match
     private int checkAll() {
         int score = 0;
         int count = 0;
         int checkTar = 0;
         int targetClearedCount = 0;
         Piece checkPiece = new Piece();
         for (int i = BOARD_HEIGHT - 1; i > 0; i--) {
            for (int j = 0; j < BOARD_WIDTH; j++) {
                if (board[j][i] != SPACE){ 
                    //re inits the values inside the loops
                    count = 0;
                    checkTar = 0;

                    checkPiece.setX(j);
                    checkPiece.setY(i);
                    checkPiece.setNumber(board[j][i]);

                    checkTar += checkForTarget(checkPiece);

                    count += checkLeft(checkPiece);
                    count += checkRight(checkPiece);
                    count += checkAbove(checkPiece);
                    count += checkBelow(checkPiece);
                    
                    //if more than 1 match was found, clears the board of all matching pieces
                    if (count > 1) {
                        board[checkPiece.getX()][checkPiece.getY()] = SPACE;
                        count += 1;
                        if(checkLeft(checkPiece) == 1) {
                            count += checkAdjacentLeft(checkPiece);
                            count -= 1;
                            board[checkPiece.getX() -1][checkPiece.getY()] = SPACE;
                        }

                        if(checkRight(checkPiece) == 1) {
                            count += checkAdjacentRight(checkPiece);
                            count -= 1;
                            board[checkPiece.getX() +1][checkPiece.getY()] = SPACE;
                        }

                        if(checkBelow(checkPiece) == 1) {
                            count += checkAdjacentBelow(checkPiece);
                            count -= 1;
                            board[checkPiece.getX()][checkPiece.getY() +1] = SPACE;
                        }

                        if(checkAbove(checkPiece) == 1) {
                            count += checkAdjacentAbove(checkPiece);
                            count -= 1;
                            board[checkPiece.getX()][checkPiece.getY() -1] = SPACE;
                        }
                    }
                    
                    if (count > 2) {
                        score += (count * 5);
                        updateCount(count);
                    }

                    if (checkTar > 0) {
                        targetClearedCount = checkTar / 20;
                        targetClearedCount = targetClearedCount * 3;
                        count += targetClearedCount;
                        score += checkTar;
                        updateCount(targetClearedCount);
                    }
                }
                dropAllLineDown();
            }
        }
        return score;
     }

     //recursively checks for matches with all adjacent pieces above the current piece
     //returns the total count of matches
     private int checkAdjacentAbove(Piece currentPiece) {
        int count = 0;
        char currentNumber = currentPiece.getNumber();
        Piece nextPiece = new Piece();
        if (checkAbove(currentPiece) == 1) {
           nextPiece.setX(currentPiece.getX());
           nextPiece.setY(currentPiece.getY() - 1);
           nextPiece.setNumber(board[nextPiece.getX()][nextPiece.getY()]);

           //adds the matching pieces above to the count
           if (currentNumber == nextPiece.getNumber()) {
               count += checkAbove(currentPiece);
               count += checkLeft(nextPiece);
               count += checkRight(nextPiece);
               count += checkAbove(nextPiece);
            }

            if (count > 1) {
               if (checkLeft(nextPiece) == 1) {
                   checkAdjacentLeft(nextPiece);
                   board[nextPiece.getX() -1][nextPiece.getY()] = SPACE;
               }
               if (checkRight(nextPiece) == 1) {
                   checkAdjacentRight(nextPiece);
                   board[nextPiece.getX() +1][nextPiece.getY()] = SPACE;
               }
               if (checkAbove(nextPiece) == 1) {
                   checkAdjacentAbove(nextPiece);
                   board[nextPiece.getX()][nextPiece.getY() -1] = SPACE;
               }
                board[nextPiece.getX()][nextPiece.getY()] = SPACE;
           }
       }

     return count;
    }

    //recursively checks for matches with all adjacent pieces below the current piece
    //returns the total count of matches
    private int checkAdjacentBelow(Piece currentPiece) {
         int count = 0;
         char currentNumber = currentPiece.getNumber();
         Piece nextPiece = new Piece();
         if (checkBelow(currentPiece) == 1) {
            nextPiece.setX(currentPiece.getX());
            nextPiece.setY(currentPiece.getY() + 1);
            nextPiece.setNumber(board[nextPiece.getX()][nextPiece.getY()]);
            
            //adds the matching pieces below to the count then checks its neigbors
            if (currentNumber == nextPiece.getNumber()) {
                count += checkBelow(currentPiece);
                count += checkLeft(nextPiece);
                count += checkRight(nextPiece);
                count += checkBelow(nextPiece);
            }

            if (count > 1) {
                if (checkLeft(nextPiece) == 1) {
                    checkAdjacentLeft(nextPiece);
                    board[nextPiece.getX() -1][nextPiece.getY()] = SPACE;
                }
                if (checkRight(nextPiece) == 1) {
                    checkAdjacentRight(nextPiece);
                    board[nextPiece.getX() +1][nextPiece.getY()] = SPACE;
                }
                if (checkBelow(nextPiece) == 1) {
                    checkAdjacentBelow(nextPiece);
                    board[nextPiece.getX()][nextPiece.getY() +1] = SPACE;
                }
                board[nextPiece.getX()][nextPiece.getY()] = SPACE;
            }
        }
         return count;
     }

     //recursively checks for matches with all adjacent pieces to the left of the current piece
     //returns the total count of matches
     private int checkAdjacentLeft(Piece currentPiece) {
        int count = 0;
        char currentNumber = currentPiece.getNumber();
        Piece nextPiece = new Piece();
        
        //checks if there is a matching piece to the left
        if (checkLeft(currentPiece) == 1) {
            nextPiece.setX(currentPiece.getX() - 1);
            nextPiece.setY(currentPiece.getY());
            nextPiece.setNumber(board[nextPiece.getX()][nextPiece.getY()]);

            //adds the matching peices to the left to the count, then checks all adjacent pieces
            if(currentNumber == nextPiece.getNumber()) {
                count += checkLeft(currentPiece);
                count += checkBelow(nextPiece);
                count += checkAbove(nextPiece);
                count += checkLeft(nextPiece);
            }

            if (count > 1) {
                if (checkLeft(nextPiece) == 1) {
                    checkAdjacentLeft(nextPiece);
                    board[nextPiece.getX() -1][nextPiece.getY()] = SPACE;
                }

                if (checkBelow(nextPiece) == 1) {
                    checkAdjacentBelow(nextPiece);
                    board[nextPiece.getX()][nextPiece.getY() +1] = SPACE;
                }

                if (checkAbove(nextPiece) == 1) {
                    checkAdjacentAbove(nextPiece);
                    board[nextPiece.getX()][nextPiece.getY() - 1] = SPACE;
                }

                board[nextPiece.getX()][nextPiece.getY()] = SPACE;
            }
        }
        return count;
     }
     
     //recursively checks for matches with all adjacent pieces to the right of the current piece
     //returns the total count of matches
     private int checkAdjacentRight(Piece currentPiece) {
        int count = 0;
        char currentNumber = currentPiece.getNumber();
        Piece nextPiece = new Piece();

        if (checkRight(currentPiece) == 1) {
            nextPiece.setX(currentPiece.getX() + 1);
            nextPiece.setY(currentPiece.getY());
            nextPiece.setNumber(board[nextPiece.getX()][nextPiece.getY()]);

            //adds the right side matching pieceto the count, then checks all adjacent pieces
            if(currentNumber == nextPiece.getNumber()) {
                count += checkRight(currentPiece);
                count += checkBelow(nextPiece);
                count += checkAbove(nextPiece);
                count += checkRight(nextPiece);
            }

            if (count > 1) {
                if (checkRight(nextPiece) == 1) {
                    checkAdjacentRight(nextPiece);
                    board[nextPiece.getX() +1][nextPiece.getY()] = SPACE;
                }

                if (checkBelow(nextPiece) == 1) {
                    checkAdjacentBelow(nextPiece);
                    board[nextPiece.getX()][nextPiece.getY() +1] = SPACE;
                }

                if (checkAbove(nextPiece) == 1) {
                    checkAdjacentAbove(nextPiece);
                    board[nextPiece.getX()][nextPiece.getY() - 1] = SPACE;
                }

                board[nextPiece.getX()][nextPiece.getY()] = SPACE; 
            }
        }
        return count;
     }

     //checks if the piece below the current piece is a match, returns 1 if true
     private int checkBelow(Piece curPiece) {
        char currentNumber = curPiece.getNumber();
        Piece checkPiece = new Piece();
        int count = 0;

        //check to avoid an out-of-bounds error
        if (curPiece.getY() + 1 < BOARD_HEIGHT) {
            checkPiece.setX(curPiece.getX());
            checkPiece.setY(curPiece.getY() + 1);
            checkPiece.setNumber(board[checkPiece.getX()][checkPiece.getY()]);
            if(currentNumber == checkPiece.getNumber()) {
                count = 1;
            }
        }
        return count;
     }

     //checks if the piece above the current piece is a match, returns 1 if true
     private int checkAbove(Piece curPiece) {
        char currentNumber = curPiece.getNumber();
        Piece checkPiece = new Piece();
        int count = 0;
        
        //check to avoid an out-of-bounds error
        if (curPiece.getY() - 1 > 0) {
            checkPiece.setX(curPiece.getX());
            checkPiece.setY(curPiece.getY() - 1);
            checkPiece.setNumber(board[checkPiece.getX()][checkPiece.getY()]);
            if(currentNumber == checkPiece.getNumber()) {
                count = 1;
            }
        }
        return count;
     }

     //checks if the piece to the right of the current piece is a match, returns 1 if true
     private int checkRight(Piece curPiece) {
        int count = 0;
        char currentNumber = curPiece.getNumber();
        Piece checkPiece = new Piece();

        //check to avoid an out-of-bounds error
        if (curPiece.getX() + 1 < BOARD_WIDTH) {
            checkPiece.setX(curPiece.getX() + 1);
            checkPiece.setY(curPiece.getY());
            checkPiece.setNumber(board[checkPiece.getX()][checkPiece.getY()]);
            if(currentNumber == checkPiece.getNumber()) {
                count = 1;
            }
        }
        return count;
     }

     //checks if the piece to the left of the current piece is a match, returns 1 if true
     private int checkLeft(Piece curPiece) {
        int count = 0;
        char currentNumber = curPiece.getNumber();
        Piece checkPiece = new Piece();

        //check to avoid an out-of-bounds error
        if (curPiece.getX() > 0) {
            checkPiece.setX(curPiece.getX() - 1);
            checkPiece.setY(curPiece.getY());
            checkPiece.setNumber(board[checkPiece.getX()][checkPiece.getY()]);
            if(currentNumber == checkPiece.getNumber()) {
                count = 1;
            }
        }
        return count;
     }

     //checks if the current piece completes a target match
     private int checkForTarget(Piece curPiece) {
        int score = 0; 
        int checkLeft = 0;
        int checkRight = 0;
        int checkAbove = 0;
        int checkBelow = 0;

        checkLeft += checkLeftTar(curPiece);
        checkRight += checkRightTar(curPiece);
        checkAbove += checkAboveTar(curPiece);
        checkBelow += checkBelowTar(curPiece);

        score += checkLeft;
        score += checkRight;
        score += checkAbove;
        score += checkBelow;

        //if the checks came back positive for a match
        if (score >= 20) {
            if(checkLeft >= 20) {
                board[curPiece.getX() -2][curPiece.getY()] = SPACE;
                board[curPiece.getX() -1][curPiece.getY()] = SPACE;
                board[curPiece.getX()][curPiece.getY()] = SPACE;
            }
            if (checkRight >= 20) {
                board[curPiece.getX() +2][curPiece.getY()] = SPACE;
                board[curPiece.getX() +1][curPiece.getY()] = SPACE;
                board[curPiece.getX()][curPiece.getY()] = SPACE;
            }
            if (checkAbove >= 20) {
                board[curPiece.getX()][curPiece.getY() -2] = SPACE;
                board[curPiece.getX()][curPiece.getY() -1] = SPACE;
                board[curPiece.getX()][curPiece.getY()] = SPACE;
            }
            if (checkBelow >= 20) {
                board[curPiece.getX()][curPiece.getY() +2] = SPACE;
                board[curPiece.getX()][curPiece.getY() +1] = SPACE;
                board[curPiece.getX()][curPiece.getY()] = SPACE;
            }
        }      
        return score;
     }

     //checks if the target string matches to the left
     private int checkLeftTar(Piece curPiece) {
        int score = 0;
        int x = curPiece.getX();
        int y = curPiece.getY(); 
        char currentNumber = curPiece.getNumber();

        //we assume that the tar string will always be 3 chars long, but
        //we allow for it to change to a different string of length 3.
        char tarString[] = new char[3];
        for (int i = 0; i < TARGET.length(); i++){
            tarString[i] = TARGET.charAt(i);
        }

        if (x > 1) {
            //check the begging of the target string
            if (currentNumber == tarString[0]) {
                if(board[x-1][y] == tarString[1]) {
                    if(board[x-2][y] == tarString[2]) {
                        score = 20;
                    }
                }
            }
        }
        return score;
     }

     //checks if the target string matches to the right
     private int checkRightTar(Piece curPiece) {
        int score = 0;
        int x = curPiece.getX();
        int y = curPiece.getY(); 
        char currentNumber = curPiece.getNumber();

        //we assume that the tar string will always be 3 chars long, but
        //we allow for it to change to a different string of length 3.
        char tarString[] = new char[3];
        for (int i = 0; i < TARGET.length(); i++){
            tarString[i] = TARGET.charAt(i);
        }

        //can we go at least two to the right?
        if (x < BOARD_WIDTH - 2) {
            if (currentNumber == tarString[0]) {
                if(board[x+1][y] == tarString[1]) {
                    if (board[x+2][y] == tarString[2]) {
                        score = 20;
                    }
                }
            }
        }
        return score;
     }

     //checks if the target string matches above
     private int checkAboveTar(Piece curPiece) {
        int score = 0;
        int x = curPiece.getX();
        int y = curPiece.getY(); 
        char currentNumber = curPiece.getNumber();

        //we assume that the tar string will always be 3 chars long, but
        //we allow for it to change to a different string of length 3.
        char tarString[] = new char[3];
        for (int i = 0; i < TARGET.length(); i++){
            tarString[i] = TARGET.charAt(i);
        }

        //can we go at least two up?
        if (y > 1) {
            if (currentNumber == tarString[0]) {
                if(board[x][y -1] == tarString[1]) {
                    if (board[x][y -2] == tarString[2]) {
                        score = 20;
                    }
                }
            }
        }
        return score;
     }

     //checks if the target string matches downard
     private int checkBelowTar(Piece curPiece) {
        int score = 0;
        int x = curPiece.getX();
        int y = curPiece.getY(); 
        char currentNumber = curPiece.getNumber();
        char tarString[] = new char[3];
        for (int i = 0; i < TARGET.length(); i++){
            tarString[i] = TARGET.charAt(i);
        }
        //can we go at least two down?
        if (y < BOARD_HEIGHT - 2) {
            if (currentNumber == tarString[0]) {
                if(board[x][y +1] == tarString[1]) {
                    if (board[x][y + 2] == tarString[2]) {
                        score = 20;
                    }
                }
            }
        }
        return score;
     }

    /* handles the key presses*/
    class TAdapter extends KeyAdapter {
        
         @Override
         public void keyPressed(KeyEvent e) {

             if (!isStarted || curPiece.getNumber() == SPACE) {  
                 return;
             }

             int keycode = e.getKeyCode();

             if (keycode == 'P') {
                 pause();
                 return;
             }

             if (isPaused)
                 return;

             switch (keycode) {
                 
             case KeyEvent.VK_LEFT:
                 tryMove(curPiece, - 1, 0);
                 break;
                 
             case KeyEvent.VK_RIGHT:
                 tryMove(curPiece,  1, 0);
                 break;
                 
             case KeyEvent.VK_DOWN:
            	 tryMove(curPiece, 0, 2);
                 break;
                 
             case KeyEvent.VK_UP:
                 tryMove(curPiece, 0, 0);
                 break;
                 
             case KeyEvent.VK_SPACE:
                 dropDown();
                 break;
                 
             case 'D':
                 oneLineDown();
                 break;
             }
         }
     }
}
