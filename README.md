# JavaTetris

As a homework assignment I created a program that functions like tetris with numbered squares.
After matching three like numbers the board clears them and is then refreshed with pieces droping down.
All matches made after pieces are dropped are also cleared. 
As a bonus, the number 355 may be cleared in a vetrical or horizontal line for extra points. 355 was the 
course number for this class.